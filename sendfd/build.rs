fn main() {
    cc::Build::new()
        .file("src/lib.c")
        .warnings(true)
        .extra_warnings(true)
        .compile("sendfd-sys.so")
}
