#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <errno.h>

int * __error(void);

int send_fildes(int socket, int *fildes, size_t fildes_len, char *data, size_t data_len)
{
    size_t fildes_size = sizeof(int) * fildes_len;

    union {
        char buf[CMSG_SPACE(fildes_size)];
        struct cmsghdr useless;
    } controlMsg;

    struct msghdr msgh;
    msgh.msg_name = NULL;
    msgh.msg_namelen = 0;

    struct iovec iov;
    iov.iov_base = data;
    iov.iov_len =  data_len;
    msgh.msg_iov = &iov;
    msgh.msg_iovlen = 1;

    msgh.msg_control = controlMsg.buf;
    msgh.msg_controllen = sizeof(controlMsg.buf);

    struct cmsghdr *cmsgp;
    cmsgp = CMSG_FIRSTHDR(&msgh);
    cmsgp->cmsg_level = SOL_SOCKET;
    cmsgp->cmsg_type = SCM_RIGHTS;
    cmsgp->cmsg_len = CMSG_LEN(fildes_size);

    memcpy(CMSG_DATA(cmsgp), fildes, fildes_size);

    return sendmsg(socket, &msgh, 0);
}

int receive_fildes(int socket, int *fildes, size_t fildes_len, char *data, size_t data_len)
{
    size_t fildes_size = sizeof(int) * fildes_len;

    union  {
        char buf[CMSG_SPACE(fildes_size)];
        struct cmsghdr useless;
    } controlMsg;

    struct msghdr msgh;
    msgh.msg_name = NULL;
    msgh.msg_namelen = 0;

    struct iovec iov;
    iov.iov_base = data;
    iov.iov_len = data_len;
    msgh.msg_iov = &iov;
    msgh.msg_iovlen = 1;
    
    msgh.msg_control = controlMsg.buf;
    msgh.msg_controllen = sizeof(controlMsg.buf);

    if (recvmsg(socket, &msgh, 0) == -1)
        return -1;

    struct cmsghdr *cmsgp = CMSG_FIRSTHDR(&msgh);
    if (cmsgp == NULL || cmsgp->cmsg_len != CMSG_LEN(fildes_size) ||
        cmsgp->cmsg_level != SOL_SOCKET || cmsgp->cmsg_type != SCM_RIGHTS) {
        errno = EINVAL;
        return -1;
    }
    
    memcpy(fildes, CMSG_DATA(cmsgp), fildes_size);
    return 0;
}
