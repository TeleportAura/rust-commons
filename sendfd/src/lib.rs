use std::io::Error;
use std::os::unix::io::{AsRawFd, FromRawFd};
use std::mem::MaybeUninit;

pub fn write(socket: &impl AsRawFd, fd: &impl AsRawFd) -> Result<(), Error> {
    let buf = [4u8];
    let sendfd = fd.as_raw_fd();
    if unsafe { send_fildes(socket.as_raw_fd(), &sendfd as *const _, 1, &buf as *const _, 1) == -1 } {
        return Err(Error::last_os_error());
    }
    Ok(())
}

pub fn read<A: FromRawFd>(socket: &impl AsRawFd) -> Result<A, Error> {
    let mut buf = [0u8];
    let mut sendfd = MaybeUninit::<i32>::uninit();
    if unsafe {  receive_fildes(socket.as_raw_fd(), sendfd.as_mut_ptr(), 1, &mut buf as *mut _, 1) } == -1 {
        return Err(Error::last_os_error());
    }
    Ok(unsafe {
        let sendfd = sendfd.assume_init();
        A::from_raw_fd(sendfd)
    })
}

pub fn write_slice(socket: &impl AsRawFd, fd: &impl AsRawFd, data:  &[u8]) -> Result<(), Error> {
    let sendfd = fd.as_raw_fd();
    if unsafe { send_fildes(socket.as_raw_fd(), &sendfd as *const _, 1, data.as_ptr(), data.len()) }  == -1 {
        return Err(Error::last_os_error());
    }
    Ok(())
}

pub fn read_slice<A: FromRawFd>(socket: &impl AsRawFd, data: &mut [u8]) -> Result<A, Error> {
    let mut sendfd = MaybeUninit::<i32>::uninit();
    if unsafe { receive_fildes(socket.as_raw_fd(), sendfd.as_mut_ptr(), 1, data.as_mut_ptr(), data.len()) } ==  -1 {
        return Err(Error::last_os_error());
    }
    Ok(unsafe {
        let sendfd = sendfd.assume_init();
        A::from_raw_fd(sendfd)
    })
}

pub fn write_with_custom_data() {}

pub fn read_with_custom_data() {}

pub fn write_slice_with_custom_data() {}

pub fn read_slice_with_custom_data() {}

extern {
    pub fn send_fildes(sockfd: i32, fildes: *const i32, fildes_len: usize, data: *const u8, data_len: usize) -> i32;

    pub fn receive_fildes(sockfd: i32, fildes: *mut i32, fildes_len: usize, data: *mut u8, data_len: usize) -> i32;
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::net::TcpListener;
    use std::os::unix::{io::IntoRawFd, net::UnixStream};

    #[test]
    fn single() {
        let (tx, rx) = UnixStream::pair().unwrap();
        let listener = TcpListener::bind("localhost:1337").unwrap();
        write(&tx, &listener);
        let listener2: TcpListener = read(&rx).unwrap();
        assert_eq!(listener.local_addr().unwrap(), listener2.local_addr().unwrap());
    }

}
